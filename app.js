//Budget Controller
var budgetController = (function () {

    var Expanse = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };

    var data = {
        allItemes: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        }
    };

    return {
        addItem: function (type, des, val) {
            var newItem, ID;

            //Create new iD

            if (data.allItemes[type].length > 0) {
                ID = data.allItemes[type][data.allItemes[type].length - 1].id + 1;
            } else {
                ID = 0;
            }

            // Create new item based on 'inc' or 'exp' type
            if (type === 'exp') {
                newItem = new Expanse(ID, des, val);
            } else if (type === 'inc') {
                newItem = new Expanse(ID, des, val);
            }

            // push it into or data structure
            data.allItemes[type].push(newItem);

            //Return the new element
            return newItem;
        },
        testing: function () {
            console.log(data);

        }

    };


})();

//UI Controller
var UIController = (function () {

    DOMstrings = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: 'income__list',
        expanseContainer: 'expanse__list'
    };

    return {
        getInput: function () {
            return {
                type: document.querySelector(DOMstrings.inputType).value,
                description: document.querySelector(DOMstrings.inputDescription).value,
                value: document.querySelector(DOMstrings.inputValue).value
            };
        },


        addListItem: function (obj, type) {

            var html, newHtml;
            // Html String with placeholder text
            if (type === 'inc') {
                element = DOMStrings.incomeContainer;
                html = '<div class = "item clearfix" id = "income-%id%"> <div class = "item__description"> %description% </div> <div class = "right clearfix"> <div class = "item__value"> %value% </div> <div class = "item__delete"><button class = "item__delete--btn"> <i class = "ion-ios-close-outline"></i></button ></div> </div> </div>';

            } else if (type === 'exp') {
                element = DOMStrings.expenseContainer;
                html = '<div class = "item clearfix" id = "expense-%id%"><div class = "item__description" > %description% </div> <div class = "right clearfix"><div class = "item__value"> %value% </div> <div class = "item__percentage" > 21 % </div> <div class = "item__delete" ><button class = "item__delete--btn"><i class = "ion-ios-close-outline"> </i></button></div> </div> </div>';
            }
            // replace the placeholder text with some actual data
            newHtml = html.replace('%id', obj.id);
            newHtml = newHtml.replace('%description%', obj.description);
            newHtml = newHtml.replace('%value%', obj.value);
            // insert the html into the DOM
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        addListItem: function (obj, type) {

        },


        getDOMstrings: function () {
            return DOMstrings;
        }

    };

})();


//Global App Controller
var controller = (function (budgetCtrl, UICtrl) {

    var setupEventListeners = function () {
        console.log('Application Started.');

        var DOM = UICtrl.getDOMstrings();

        document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });
    };

    var ctrlAddItem = function () {

        var input, newItem;

        //Get the field input data
        input = UICtrl.getInput();

        // Add the item to the budget controller
        newItem = budgetCtrl.addItem(input.type, input.description, input.value);

        // Add the item to the UI

        // Calculate the budget

        // Display the budget on the UI 

    };

    return {
        init: function () {

            setupEventListeners();

        }
    };

})(budgetController, UIController);

controller.init();